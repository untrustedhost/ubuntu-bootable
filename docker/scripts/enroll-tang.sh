#!/usr/bin/env bash

set -e

kv="$(uname -r)"

# this code is snagged from the...partition resizer
mountpt="/"
target=''
parent=''
luksdev=''

# find device for mountpoint
while read -r line ; do
  read -r device mount fstype rest <<<$line
  [[ "${mount}" == "${mountpt}" ]] && { target="${device}" ; break ; }
done < /proc/mounts

# check if any parents are luks before continuing.
while read -r line ; do
  [[ "${luksdev}" ]] && continue
  ttype='' ; tname='' majmin='' ; parts=()
  read -ra parts <<<$line
  for p in "${parts[@]}" ; do
    case "${p}" in
      TYPE=*) ttype="${p#TYPE=\"}" ; ttype="${ttype%\"}" ;;
      NAME=*) tname="${p#NAME=\"}" ; tname="${tname%\"}" ;;
      MAJ:MIN=*) majmin="${p#MAJ:MIN=\"}" ; majmin="${majmin%\"}" ;;
    esac
  done
  case "${ttype}" in
    crypt)
      # are you _really_?
      case "$(file -s "/dev/${parent}")" in
        *LUKS*) luksdev="/dev/${parent}" ;;
        *) : ;;
      esac
     ;;
    *) parent="${tname}" ;;
  esac
done < <(lsblk -Ps "${target}"|tac)

[[ "${luksdev}" ]] || {
  printf "no LUKS device for enrollment of / filesystem found" 1>&2
  exit 1
}

# 20.04 (and lower, likely) needs the _netdev flag for dracut ordering
case "$(augtool get '/files/etc/fstab/*[file="/"]/opt[.="_netdev"]')" in
  # _netdev is already set
  *=*=*=*) : ;;
  *)
    printf '%s\n' \
      'ins opt after /files/etc/fstab/*[file="/"]/opt[last()]' \
      'set /files/etc/fstab/*[file="/"]/opt[last()] _netdev' \
      'save' | augtool
  ;;
esac

# bind to a tang server
clevis luks bind -d "${luksdev}" tang "{\"url\":\"http://${1}\"}"

# remove default tang disable if there, rebuild initrd.
rm -f /etc/dracut.conf.d/00-DISABLE_clevis-pin-tang.conf

cp "/boot/initrd.img-${kv}" "/boot/initrd.img.preclevis-${kv}"

dracut -f "/boot/initrd.img-${kv}" "${kv}"
