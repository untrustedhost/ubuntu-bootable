#!/usr/bin/env bash

# remove old embedded EFI image
rm /boot/efiboot.img

# install grub EFI-style...
mkdir -p /boot/efi/EFI/ubuntu/
cp -R /usr/lib/grub/{i386,x86_64}-efi /boot/efi/EFI/ubuntu/
cp -R /usr/lib/grub/{i386,x86_64}-efi /boot/grub/

# give efi grub a config to load from /boot
bootdev=$(awk '$2 == "/boot" { print $1 ; }' < /proc/mounts)
bootuuid=$(blkid -s UUID -o value "${bootdev}")
{
  echo "insmod part_gpt"
  echo "insmod ext2"
  echo "search --set --fs-uuid ${bootuuid}"
  echo "configfile /grub/grub.cfg"
} > /boot/efi/EFI/ubuntu/grub.cfg

# deduce serial console at this point
[ "${CONFIG_SERIAL_INSTALL}" ] || CONFIG_SERIAL_INSTALL=$(tty)

case "${CONFIG_SERIAL_INSTALL}" in
  /dev/ttyS*) CONFIG_SERIAL_INSTALL="${CONFIG_SERIAL_INSTALL#/dev/ttyS}" ;;
  /dev/*)     unset CONFIG_SERIAL_INSTALL ;;
  [0-9]*)     : ;;
  *)
    echo "CONFIG_SERIAL_INSTALL should be set to the unit number of the serial port (likely 0 or 1)" 1>&2
    unset CONFIG_SERIAL_INSTALL
  ;;
esac

# usbser works differently enough to be maddening, sorry!
# mostly because _we don't do much of it here_ - it's in the initrd generator and finalizer!
[ "${CONFIG_USBSER_INSTALL}" ] || CONFIG_USBSER_INSTALL=$(tty)

case "${CONFIG_USBSER_INSTALL}" in
  /dev/ttyUSB*) CONFIG_USBSER_INSTALL="${CONFIG_USBSER_INSTALL#/dev/ttyUSB}" ;;
  /dev/*)       unset CONFIG_USBSER_INSTALL ;;
  [0-9]*)       : ;;
  *)
    echo "CONFIG_USBSER_INSTALL should be set to the unit number of the USB serial port" 1>&2
    unset CONFIG_USBSER_INSTALL
  ;;
esac

[ -n "${CONFIG_SERIAL_INSTALL}" ] && {
  {
    printf 'GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=%s --word=8 --parity=no --stop=1"\n' "${CONFIG_SERIAL_INSTALL}"
    printf 'GRUB_TERMINAL="serial"\n'
  } >> /etc/default/grub
  cmdline=$(augtool print /files/etc/default/grub/GRUB_CMDLINE_LINUX_DEFAULT)
  cmdline="${cmdline#* = }"
  cmdline="${cmdline/splash/}"
  # since we know the _last_ three characters of this are quoting, splice like this
  if [ -n "${cmdline}" ] ; then
    cmdline="${cmdline:0:-3} console=ttyS${CONFIG_SERIAL_INSTALL},115200n8${cmdline: -3}"
  else
    cmdline="console=ttyS${CONFIG_SERIAL_INSTALL},115200n8"
  fi
  augtool -s set /files/etc/default/grub/GRUB_CMDLINE_LINUX_DEFAULT "${cmdline}"
}

[ -n "${CONFIG_USBSER_INSTALL}" ] && {
  cmdline=$(augtool print /files/etc/default/grub/GRUB_CMDLINE_LINUX_DEFAULT)
  # all we're doing is removing splash, really
  cmdline="${cmdline#* = }"
  cmdline="${cmdline/splash/}"
  # since we know the _last_ three characters of this are quoting, splice like this
  augtool -s set /files/etc/default/grub/GRUB_CMDLINE_LINUX_DEFAULT "${cmdline}"
}

# make a BIOS grub config
grub-mkconfig -o /boot/grub/grub.cfg

# install grub now
BIOS_BOOTDEVS="/dev/sda"
# shellcheck source=/dev/null
[ -f /root/fs-env ] && source /root/fs-env
for d in ${BIOS_BOOTDEVS} ; do
  grub-install ${d%%[0-9]*}
done
