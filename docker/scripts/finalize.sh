#!/usr/bin/env bash

set -e

# perparing for image to meaningfully work in non-bootloading ways.

# toto we're not in docker anymore.
rm -f /.dockerenv

# use systemd-resolve resolv.conf plz
ln -sf ../run/systemd/resolve/resolv.conf /etc/resolv.conf

# wipe machine-id
: > /etc/machine-id
: > /var/lib/dbus/machine-id

# try grabbing the hw id
/usr/lib/untrustedhost/scripts/machine-hw-id.sh /etc/machine-hw-id

# if you are on usb serial, configure a console service for later.
[ "${CONFIG_USBSER_INSTALL}" ] || CONFIG_USBSER_INSTALL=$(tty)

case "${CONFIG_USBSER_INSTALL}" in
  /dev/ttyUSB*) CONFIG_USBSER_INSTALL="${CONFIG_USBSER_INSTALL#/dev/ttyUSB}" ;;
  /dev/*)       unset CONFIG_USBSER_INSTALL ;;
  [0-9]*)       : ;;
  *)
    echo "CONFIG_USBSER_INSTALL should be set to the unit number of the USB serial port" 1>&2
    unset CONFIG_USBSER_INSTALL
  ;;
esac

[ -n "${CONFIG_USBSER_INSTALL}" ] && {
  systemctl enable "serial-getty@ttyUSB${CONFIG_USBSER_INSTALL}"
}

# pregenerate systemd-networkd link files
/usr/lib/untrustedhost/scripts/platform-info.sh gennetlinks
