#!/bin/sh

init_integritydm(){
  local diskdev line word uuid
  info "Initializing dm-integrity devices"
  blkid -t TYPE="DM_integrity" | while read -r line ; do
    diskdev='' ; uuid=''
    for word in ${line} ; do
      case "${word}" in
        /dev/*) diskdev=$(echo "${word}" | sed -e 's@/dev/@@' -e 's@:@@') ;;
	PARTUUID=*) uuid=$(echo "${word}" | sed -e 's@PARTUUID=@@' -e 's@"@@g') ;;
      esac
    done
    # we have device holders, carry on.
    ls -1qA "/sys/class/block/${diskdev}/holders/" | grep -q . && continue
    # try init
    integritysetup open "/dev/${diskdev}" "integ-${uuid}"
  done | vinfo
  return "${PIPESTATUS[0]}"
}

init_integritydm
