#!/bin/bash

# called by dracut
check() {
    require_binaries integritysetup blkid cut || return 1
    # no integ volumes?
    blkid -t TYPE=DM_integrity || return 255
}

# called by dracut
depends() {
    return 0
}

# called by dracut
installkernel() {
    return 0
}

# called by dracut
install() {
    inst $(command -v integritysetup) /sbin/integritysetup
    inst $(command -v blkid) /sbin/blkid
    inst $(command -v cut) /bin/cut
    inst $(command -v grep) /bin/grep
    inst $(command -v sed) /bin/sed

    inst_hook initqueue/finished 20 "$moddir/integrity-volumes.sh"
}
