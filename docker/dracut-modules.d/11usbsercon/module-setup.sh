#!/usr/bin/env bash

check() {
  [ -f /etc/dracut-usbcon.conf ] || return 1
  return 0
}

depends() {
  echo usbser
  return 0
}

install() {
  read -r port < /etc/dracut-usbcon.conf
  inst agetty
  inst_simple "${moddir}/serial-emergency@.service" "$systemdsystemunitdir/serial-emergency@.service"
  inst_simple "${moddir}/systemd-ask-password-serial@.service" "$systemdsystemunitdir/systemd-ask-password-serial@.service"
  printf 'ACTION=="add", KERNEL=="%s", TAG+="systemd", ENV{SYSTEMD_WANTS}+="systemd-ask-password-serial@%%k.service"\n' "${port}" > "$initdir/etc/udev/rules.d/10-usbserial.rules"
  systemctl --root "$initdir" enable "serial-emergency@${port}.service"
  return 0
}
