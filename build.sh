#!/usr/bin/env bash

# resolve desired imageref and call it ubuntu:latest

[ "${VARIANT:-}" ] || VARIANT="latest"
[ "${BASE_IMAGE_REFERENCE:-}" ] || BASE_IMAGE_REFERENCE="registry.gitlab.com/untrustedhost/baseimages/ubuntu:${VARIANT}"

__warn_msg () { echo "${@}" 1>&2 ; }

# get checkout directory git revision information
__get_cr () {
  local cr
  # initial shortrev
  cr="$(git rev-parse --short HEAD)"
  # check for uncommitted files
  { git diff-index --quiet --cached HEAD -- &&
    # check for extra files
    git diff-files --quiet ; } || cr="${cr}-DIRTY"
  echo "${cr}"
}

# derive build stamps here
[ -n "${CODEBASE}" ] || { __warn_msg "CODEBASE not set" ; exit 1 ; }
CODEREV="$(__get_cr)"
TIMESTAMP="$(date +%s)"

case "${CODEREV}" in
  *-DIRTY) __warn_msg "WARNING: git tree is dirty, sleeping 5 seconds for running confirmation."
           sleep 5
           ;;
esac

# copy vendor pieces over
( cd vendor/RemixVSL/iomemory-vsl-greydawn && git archive --format=tar HEAD:root/usr/src/iomemory-vsl-3.2.16 ) > docker/vendor/iomemory-vsl-greydawn.tar
( cd vendor/RemixVSL/iomemory-vsl-main && git archive --format=tar HEAD:root/usr/src/iomemory-vsl-3.2.16 ) > docker/vendor/iomemory-vsl-main.tar
( cd vendor/intel/ucode && git archive --format=tar HEAD:intel-ucode ) > docker/vendor/intel-ucode.tar

# imd produces a selfextractor, so
top="$(pwd)"
( cd vendor/untrustedhost/imd && make "OUTPUTDIR=${top}/docker/vendor/imd/" )

echo "building ${CODEREV} at ${TIMESTAMP}"

{
  echo "${CODEBASE}_image_coderev=${CODEREV}"
  echo "${CODEBASE}_image_timestamp=${TIMESTAMP}"
} | tee "docker/facts.d/${CODEBASE}.txt"

docker pull "${BASE_IMAGE_REFERENCE}"
docker tag  "${BASE_IMAGE_REFERENCE}" 51d1bf33-6d80-4e32-981c-f015a93fc03f

# build
docker build -t "build/${VARIANT}/release" docker
